基础版
1.讲解分布式框架的基本情况和视频录制计划
2.讲解zscat分布式框架的由来
3.讲解单机版zscat框架的代码生成和权限配置
4.下载开源版分布式框架及部署运行
5.演示zscat分布式框架的功能介绍
6.zscat分布式框架的demo级开发
7。怎么开发一个新的模块
8.框架代码简单介绍
9.logback讲解和封装


高级版
8.zscat分库分表-日志模块分析 用sharding-jdbc组件
9.安装部署kafka的使用
10.单机版storm的使用
11.kafka整合storm
12.测试日志导入了kafka，经过storm流式处理数据导入redis ，luence，mysql
13.elk的简单介绍和安装
14.利用elk收集各模块的数据
